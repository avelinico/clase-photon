﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class ControlJuegoscrip : MonoBehaviourPun
{
    public GameObject Enemigo;
    public int maxNumEnemigo = 3;
    public int NumEnemigo = 0;
    public GameObject[] SpawnE;
    public float tiempoSpawn;
    private float tiempodescuento;
    public bool EmpiezaEljuego=false;
    // Start is called before the first frame update
    void Start()
    {

        //if (!PhotonNetwork.IsMasterClient) Destroy(this);

        SpawnE = GameObject.FindGameObjectsWithTag("SpawnPoint");
        tiempodescuento = tiempoSpawn;
    
    }

    // Update is called once per frame
    void Update()
    {

        if (!PhotonNetwork.IsMasterClient) return;


        if (EmpiezaEljuego)
        {
            tiempodescuento -= Time.fixedDeltaTime;
            if (tiempodescuento <= 0)
            {
                if (NumEnemigo < maxNumEnemigo)
                {
                    NumEnemigo++;
                    int pos = Random.Range(0, SpawnE.Length);
                    PhotonNetwork.Instantiate(Enemigo.name, SpawnE[pos].transform.position, Quaternion.identity, 0);
                    print("Instancio Enemigo");

                }

                tiempodescuento = tiempoSpawn;

            }
        }

        

       

    }


}
