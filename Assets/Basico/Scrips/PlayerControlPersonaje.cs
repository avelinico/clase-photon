﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerControlPersonaje : MonoBehaviourPun
  
{
    public ControlPersonajeç Owner;
    public Transform puntocamara;
    private void Awake()
    {
        Owner = this.gameObject.GetComponent<ControlPersonajeç>();

    }
    // Start is called before the first frame update
    void Start()
    {
        if (!photonView.IsMine) 
        {
            Destroy(this);
            Destroy(GetComponent<Rigidbody>());
        }
        else
        {
            gameObject.name = "Personaje Local";
            Camera.main.transform.SetParent(puntocamara, true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump")) Owner.Salta();
        if (Input.GetButton("Horizontal"))
        {

            Owner.Giro(Input.GetAxis("Horizontal") > 0 ? 1 : -1);

        }
        if (Input.GetButton("Vertical"))
        {

            Owner.Muevete(Input.GetAxis("Vertical") > 0 ? 1 : -1);

        }

        if (Input.GetButtonDown("Fire1"))
        {
            photonView.RPC("Disparo",RpcTarget.All);
        }
    }
}
