﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using Photon.Pun;

public class Character : MonoBehaviourPun,IPunObservable
{
    /*VARIABLES CREADAS POR EL PROFESOR.*/
    //private:
    private Rigidbody _rb;
    private Vector3 _gravityVector;
    private float _gravityMultiplier = 1;
    private Transform _barrel;
    private Animator _barrelAnim;
    private Transform _top;
    private Transform _phBullet;
    private bool _canShoot;
    private float _timeToCanShoot;
    private RectTransform _uiContainer;
    private Image _energyBar;
    private Text _txtName;
    private float _starterLife;
    private AudioSource _comboPlayer;
    private AudioSource _fxsPlayer;

    //public:
    public float speed;
    public float speedRotation;
    public GameObject prefabParticleShoot;
    public GameObject prefabBullet;
    public GameObject prefabFXExplosion;
    public float fireRate;
    public float life;
    public float damage;
    public Vector3 targetToAim;
    public string playerName;
    public AudioClip[] sndComboKill;
    public AudioClip sndFxShoot;
    public AudioClip sndFxExplode;
    [HideInInspector]public float timeToComboKill;
    [HideInInspector]public int comboKillAmount;
    /************************************/

    private void Awake()
    {
        //Obtenemos las referencias de las partes del tanque.
        _rb = this.GetComponent<Rigidbody>();
        _top = this.transform.Find("top");
        _barrel = _top.Find("mesh/barrel");
        _phBullet = _barrel.Find("PHBullet");
        _barrelAnim = _barrel.GetComponent<Animator>();
        _energyBar = this.transform.Find("TankUI/FilledBar").GetComponent<Image>();
        _txtName = this.transform.Find("TankUI/TxtName").GetComponent<Text>();
        _comboPlayer = Camera.main.GetComponent<AudioSource>();
        _fxsPlayer = this.GetComponent<AudioSource>();

        //Ponemos la UI dentro del Canvas.
        _uiContainer = this.transform.Find("TankUI").GetComponent<RectTransform>();
        _uiContainer.SetParent(GameObject.Find("Canvas").transform);

        //Seteamos los valores iniciales.
        _canShoot = true;
        //_txtName.text = playerName;
        _starterLife = life;
    }

    private void Start()
    {
        Respawn();
    }

    /// <summary>
    ///     Se ejecuta al destruirse. Lo usamos para destruir la UI.
    /// </summary>
    private void OnDestroy()
    {
        if(_uiContainer)
            GameObject.Destroy(_uiContainer.gameObject);
    }

    public void PlayComboSound()
    {
        comboKillAmount = Mathf.Clamp(++comboKillAmount, 0, sndComboKill.Length-1);

        _comboPlayer.clip = sndComboKill[comboKillAmount];
        _comboPlayer.Play();
    }

    //Se llama al pintar la pantalla...
    void Update ()
	{
        if (_rb)
            _gravityVector.y = _rb.velocity.y * _gravityMultiplier;

        //Tiempo para volver a disparar...
        if(!_canShoot)
        {
            _timeToCanShoot -= Time.deltaTime;
            _canShoot = _timeToCanShoot <= 0;
        }

        //Actualizamos la posición de la UI.
        _uiContainer.position = RectTransformUtility.WorldToScreenPoint(Camera.main, this.transform.position);

        //Actualizamos la vida en pantalla.
        _energyBar.fillAmount = life / _starterLife;

        //Restamos el tiempo de combo.
        if (timeToComboKill > 0)
            timeToComboKill -= Time.deltaTime;
        else
            comboKillAmount = 0;

        //Hacemos que apunte.
        AimAt(targetToAim);
    }

    /// <summary>
    ///     Se ejecuta por cada iteración de la física.
    /// </summary>
    private void FixedUpdate()
    {
        if(_rb)
            _rb.velocity = _gravityVector;
    }

    /// <summary>
    ///     Mueve al personaje hacia SU adelante o SU atrás, dependiendo del valor pasado.
    /// </summary>
    /// <param name="forwardAxisValue"></param>
    public void Move(float forwardAxisValue)
    {
        //this.transform.position += Vector3.forward * speed * Time.deltaTime;
        _gravityVector = this.transform.forward * speed * forwardAxisValue;
        _gravityVector.y = _rb.velocity.y;
    }

    /// <summary>
    ///     Rota el tanque X grados.
    /// </summary>
    /// <param name="rotation"></param>
    public void Rotate(float rotation)
    {
        print("rotando");
        _rb.AddTorque(0, rotation * speedRotation * Time.fixedDeltaTime, 0, ForceMode.VelocityChange);
    }

    /// <summary>
    ///     Hace que el tanque apunte a un punto.
    /// </summary>
    /// <param name="point"></param>
    public void AimAt(Vector3 point)
    {
        var targetRotation = Quaternion.LookRotation((point - this.transform.position).normalized);
        var targetRotationVector = targetRotation.eulerAngles;
        
        _top.rotation = Quaternion.Euler(0, targetRotationVector.y, 0);
    }

    /// <summary>
    ///     Dispara.
    /// </summary>
    /// 

        [PunRPC]
    public void Shoot()
    {
        if(_canShoot)
        {
            var particlesShoot = GameObject.Instantiate(prefabParticleShoot);

            //Creamos la bullet y seteamos quién la disparó.
            var bullet = GameObject.Instantiate(prefabBullet).GetComponent<Bullet>();
            bullet.transform.position = _phBullet.position;
            bullet.transform.forward = _barrel.forward;
            bullet.owner = this;

            //Destruimos la partícula en un tiempo.
            GameObject.Destroy(particlesShoot, 1f);
            particlesShoot.transform.position = _phBullet.position;

            //Ignoramos la colisión entre este tanque y este disparo.
            Physics.IgnoreCollision(bullet.GetComponent<Collider>(), this.GetComponent<Collider>(), true);

            //Seteamos valores para que vuela a disparar.
            _canShoot = false;
            _timeToCanShoot = 1 / fireRate;

            //Animamos el barrel dependiendo del fire rate.
            _barrelAnim.speed = fireRate;
            _barrelAnim.Play("Shoot");

            //Reproducimos el sonido.
            _fxsPlayer.clip = sndFxShoot;
            _fxsPlayer.Play();
        }
    }

    /// <summary>
    ///     Daña a este personaje.
    /// </summary>
    /// <param name="damage"></param>
    public bool TakeDamage(float damage)
    {
        //Sólo puedo hacerme yo daño...

        if (photonView.IsMine)
        {
            life -= damage;
            if (life <= 0)
            {
                //Die(); //=> Debo avisar a todos... 
                photonView.RPC("Die", RpcTarget.All);
                
                Invoke("Respawn", 3);
                return true;
            }
        }
        
       

        return false;
    }

    /// <summary>
    ///     "Mata" a este tanque (en realidad sólo lo tira en otro lado).
    /// </summary>
    /// 
    [PunRPC]
    public void Die()
    {
        //Creamos la explosión.
        var fx = GameObject.Instantiate(prefabFXExplosion);
        fx.transform.position = this.transform.position;

        //Lo llevamos a una posición lejos como para no destruirlo.
        this.transform.position = new Vector3(1000, 1000, 1000);

        //Reproducimos el sonido.
        _fxsPlayer.clip = sndFxExplode;
        _fxsPlayer.Play();

        if(_rb)
            _rb.constraints = RigidbodyConstraints.FreezeAll;
    }

    /// <summary>
    ///     Hace que reaparezca este personaje.
    /// </summary>
    public void Respawn()
    {
        var allSpawnPoints = GameObject.Find("AllSpawnPoints").transform;
        this.transform.position = allSpawnPoints.GetChild(UnityEngine.Random.Range(0, allSpawnPoints.childCount)).position;

        _rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        life = _starterLife;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(targetToAim);
            stream.SendNext(speed);
            stream.SendNext(life);
        }


        if (stream.IsReading)
        {

            targetToAim = (Vector3)stream.ReceiveNext();
            speed = (float)stream.ReceiveNext();
            life = (float)stream.ReceiveNext();
        }
    }


    

}
