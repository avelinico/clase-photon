﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class NetManager1 : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
      PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnected()
    {
        print("Conectado a photon");
    }

    public override void OnConnectedToMaster()
    {
        print("Conectado al servidor");
        //Room Options se hereda de photon realtime

        var options = new RoomOptions();
        options.MaxPlayers = 10;
        PhotonNetwork.JoinOrCreateRoom("Axis",options,TypedLobby.Default);
    }


    public override void OnCreatedRoom()
    {
        print("Creo una sala");
    }
    public override void OnJoinedRoom()
    {
        print("Me uno a ala sala");
        PhotonNetwork.Instantiate("Toon Tank",Vector3.zero,Quaternion.identity,0);

    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
