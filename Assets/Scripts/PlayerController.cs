﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class PlayerController : MonoBehaviourPun
{
    public float cameraDistance;

    private Character _owner;
    private Camera _characterCam;

    //Se ejecuta al inicializar...
    void Start ()
	{
        _owner = this.GetComponent<Character>();
        _characterCam = Camera.main;

        //- "SI NO SOY YO DESTRUIME!".
        //- "Y... CÓMO SE QUE NO SOS VOS?".
        if (!photonView.IsMine)
        {
            Destroy(this);
            Destroy(this.GetComponent<Rigidbody>());
        }

	}

    /// <summary>
    ///     Se ejecuta después de cada Update. Se suele usar para la cámara, para que pinte
    ///     las posiciones correctas.
    /// </summary>
    private void LateUpdate()
    {
        _characterCam.transform.position = this.transform.position + Vector3.up * cameraDistance;
        _characterCam.transform.LookAt(this.transform);
    }

    //Se llama al pintar la pantalla...
    void Update ()
	{
        //Hacia adelante y atrás...
        _owner.Move(Input.GetAxis("Vertical"));

        //Rotación...
        if (Input.GetButton("Horizontal"))
        {
            
            _owner.Rotate(Input.GetAxis("Horizontal") > 0 ? 1 : -1);

        }
            

        //Apuntar...
        Ray ray = _characterCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if(Physics.Raycast(ray, out hitInfo))
        {
            _owner.targetToAim = hitInfo.point;
        }

        //Disparar...
        if(Input.GetButtonDown("Fire1"))
        {
            //_owner.Shoot();
            //PhotonView photonView = PhotonView.Get(this);
            photonView.RPC("Shoot",RpcTarget.MasterClient );
        }
	}

  
}
