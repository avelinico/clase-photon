﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Valve.VR;

public class ControlPersonajeGoblal : MonoBehaviourPun, IPunObservable

{
    public SteamVR_Action_Boolean Gatillo = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");
    // Start is called before the first frame update
    public GameObject VR;
    public GameObject Personaje;
    public GameObject Cabezaphoton;
    public GameObject GafasVR;
    public GameObject CabezaPersonaje;
    public GameObject Manoderecha;
    public GameObject ManoIzquierda;
    public Transform Pistoladerecha;
    public Transform PistolaIzquierda;
    public bool actderecha;
    public bool actizquierda;
    public Transform PuntodedisparoD;
    public GameObject Prebala;

    void Start()
    {
        if (photonView.IsMine)
        {
            print("es local activo vr");
            ActivoPersona(true);
            gameObject.name = " AA VR";
        }
        else
        {
            print("NO es local activo desactivo vr");
            ActivoPersona(false);
            gameObject.name = " AA INTRUSO";
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {

            if (Gatillo.GetStateDown(SteamVR_Input_Sources.RightHand))
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    GameObject.Find("ControlJuego").GetComponent<ControlJuegoscrip>().EmpiezaEljuego = true;
                }


                print("presiono Gatillo");
                photonView.RPC("Disparo", RpcTarget.All);
            }


        }

            

        ActivacionManos();

    }


    public void ActivoPersona(bool act)
    {
        if (act)
        {
            // VR ACTIVO
            VR.SetActive(true);
            Personaje.SetActive(false);
            Cabezaphoton.transform.SetParent(GafasVR.transform);
            Cabezaphoton.transform.localPosition = Vector3.zero;
            Manoderecha.transform.SetParent(Pistoladerecha);
            Manoderecha.transform.localPosition = Vector3.zero;
            ManoIzquierda.transform.SetParent(PistolaIzquierda);
            ManoIzquierda.transform.localPosition = Vector3.zero;


        }
        else
        {
            //Desactivo VR
            VR.SetActive(false);
            Personaje.SetActive(true);
            CabezaPersonaje.transform.SetParent(Cabezaphoton.transform);
            CabezaPersonaje.transform.localPosition = Vector3.zero;
        }
    }

    public void ActivacionManos()
    {
        if (photonView.IsMine)
        {
            if (Pistoladerecha.localPosition != Vector3.zero)
            {
                actderecha = true;

            }
            else
            {

                actderecha = false;
            }


            if (PistolaIzquierda.localPosition != Vector3.zero)
            {
                actizquierda = true;
            }
            else
            {
                actizquierda = false;
            }
        }

        Manoderecha.SetActive(actderecha);
        ManoIzquierda.SetActive(actizquierda);


    }


    
    [PunRPC]
    public void Disparo()
    {
        if (!actderecha) return;
        print("disparo");
        var bala = GameObject.Instantiate(Prebala);
        //bala.transform.position = PuntodedisparoD.position;
        bala.transform.position = Manoderecha.transform.position+ Manoderecha.transform.forward*0.5f;
        //bala.transform.localPosition = PuntodedisparoD.position + PuntodedisparoD.forward*2;
        bala.transform.forward = Manoderecha.transform.forward;

    }




    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            print("envio datos");
            stream.SendNext(actderecha);
            stream.SendNext(actizquierda);
        }


        if (stream.IsReading)
        {
            print("recivo");
            this.actderecha = (bool)stream.ReceiveNext();
            this.actizquierda= (bool)stream.ReceiveNext();
        }
    }


}
